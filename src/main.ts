import './custom-hooks/hooks';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import axios from 'axios';
import VueAxios from 'vue-axios';


// npm install @mathieustan/vue-datepicker --save
import { VueDatePicker } from '@mathieustan/vue-datepicker';
// https://github.com/brockpetrie/vue-moment#readme
import VueMoment from 'vue-moment';
import moment from 'moment';
import 'moment/locale/es';
// https://github.com/troxler/vue-headful
import vueHeadful from 'vue-headful';
// https://github.com/dzwillia/vue-simple-progress/
import ProgressBar from 'vue-simple-progress';
// este componente se debe importar en cada fichero propio donde se vaya a usar
// import VueProgressCircle from 'vue2-circle-progress';
// https://github.com/paulcollett/vue-dummy
import VueDummy from 'vue-dummy';

// podemos importar todo leaflet de golpe, o los componentes necesarios
// import * as Vue2Leaflet from 'vue2-leaflet';
import { LMap, LTileLayer, LMarker, LPopup, LGeoJson, LIconDefault } from 'vue2-leaflet';
import Vue2LeafletMarkerCluster from 'vue2-leaflet-markercluster';
import { Icon } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';
import 'leaflet.markercluster.freezable';

import VTooltip from 'v-tooltip';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faUserSecret, faSkullCrossbones, faCheck, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faUserSecret, faSkullCrossbones, faCheck, faMapMarkerAlt);

Vue.component('fa-icon', FontAwesomeIcon);

Vue.use(VueAxios, axios);

Vue.component('vue-date-picker', VueDatePicker);
Vue.use(VueMoment, {
  moment
});
console.info('locale', (Vue as any).moment().locale());

Vue.component('v-meta', vueHeadful);
Vue.component('v-progress-bar', ProgressBar);
// Vue.component('v-progress-circle', VueProgressCircle);
Vue.use(VueDummy);

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-popup', LPopup);
Vue.component('l-geo-json', LGeoJson);
Vue.component('l-icondefault', LIconDefault);
Vue.component('l-marker-cluster', Vue2LeafletMarkerCluster);

// delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  // iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  // iconUrl: require('leaflet/dist/images/marker-icon.png'),
  // shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

// si se quiere cambiar la configuracion de los tooltips ->
// import config from '@/assets/components-config/v-tooltip.conf';
// Vue.use(VTooltip, config);
Vue.use(VTooltip);

Vue.config.productionTip = false;

// vue-filters
import currencyFilter from '@/filters/currency-filter.ts';
Vue.filter('currency', currencyFilter);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
