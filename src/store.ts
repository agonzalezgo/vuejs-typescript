import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    prueba: 'hola, probando!!',
    // spinner state
    loading: false,
    // darkmode state
    darkmode: false,
  },
  mutations: {
    pruebaMutation: (state, val) => {
      state.prueba = val;
    },
    loadingMutation: (state) => {
      state.loading = !state.loading;
    },
    darkmodeMutation: (state, val) => {
      state.darkmode = val;
    },
  },
  actions: {
    actionPrueba({commit}, val) {
      commit('pruebaMutation', val);
    },
    actionLoading({commit}) {
      commit('loadingMutation');
    },
    actionDarkmode({commit}, val) {
      commit('darkmodeMutation', val);
    }
  },
  getters: {
    pruebaGetter: (state) => state.prueba,
    isLoading: (state) => state.loading,
    isDarkmodeActive: (state) => state.darkmode,
  },
});
