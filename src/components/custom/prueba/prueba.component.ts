import Vue from 'vue';
import Component from 'vue-class-component';

@Component({})
export default class PruebaComponent extends Vue {

  private mounted() {
    console.log('hello from app');
  }
}

