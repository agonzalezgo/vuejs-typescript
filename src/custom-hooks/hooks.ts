import Component from 'vue-class-component';

Component.registerHooks([
  'beforeRouteEnter', // se dispara antes de entrar a una ruta (necesita Vue-router)
  'beforeRouteLeave', // se dispara antes de cambiar de ruta (necesita Vue-router)
  'beforeRouteUpdate',
]);
