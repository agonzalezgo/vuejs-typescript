// const RULES = {
//   "USD": {
//     "symbol": "$",
//     "thousandSeperator": 0,
//     "decimalSeperator": 0,
//     "negativePattern": 2
//   },
//   "CAD": {
//     "symbol": "$_",
//     "thousandSeperator": 0,
//     "decimalSeperator": 0,
//     "negativePattern": 2
//   },
//   "EUR": {
//     "symbol": "€_",
//     "thousandSeperator": 1,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   },
//   "GBP": {
//     "symbol": "£_",
//     "thousandSeperator": 1,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   },
//   "JPY": {
//     "symbol": "¥_",
//     "thousandSeperator": 0,
//     "decimalSeperator": 0,
//     "negativePattern": 0
//   },
//   "DEU": {
//     "symbol": "_€",
//     "thousandSeperator": 1,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   },
//   "BRL": {
//     "symbol": "R$_",
//     "thousandSeperator": 1,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   },
//   "FRA": {
//     "symbol": "_€",
//     "thousandSeperator": 2,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   },
//   "ITA": {
//     "symbol": "€_",
//     "thousandSeperator": 1,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   },
//   "CHE": {
//     "symbol": "fr. _",
//     "thousandSeperator": 3,
//     "decimalSeperator": 0,
//     "negativePattern": 0
//   },
//   "BGR": {
//     "symbol": "_лв.",
//     "thousandSeperator": 2,
//     "decimalSeperator": 1,
//     "negativePattern": 0
//   }
// };

const currencies: any = {
  USD: {
    s: '$',
    m: ',',
    d: '.',
    placeBefore: true,
    base: {
      m: ',',
      d: '.',
    }
  },
  EUR: {
    s: '€',
    m: '.',
    d: ',',
    placeBefore: false,
    base: {
      m: '.',
      d: ',',
    }
  }
};

const C = ',';
const P = '.';
const S = ' ';
const T = '|';

const reverseString = (el: any) => el.split('').reverse().join('');
const fixDecimals = (val: string, len: number = 2) => {
  // suponemos que el valor tiene sentido... algo como 12345
  let integer = val.substr(0, len);
  let aux = 0;
  if (parseInt(val.charAt(len), 10) >= 5) {
    aux = parseInt(integer, 10);
    integer = String(aux === 99 ? aux : aux + 1);
  }
  return integer.length === 1 ? '0' + integer : integer;
};

export default function currencyFilter(value: number | string, currency: string, options?: any) {
  const _value = value.toString();
  if (isNaN(parseInt(_value, 10))) {
    // throw new TypeError('"value" has to be a string or a number');
    // pensar si es mejor lanzar error o mostrarlo por pantalla como i18n...
    return '"value" has to be a number wrapped in a string or a number';
  }
  const _currency = currency.toUpperCase();
  const use = {
    decimal: options && options.decimal ? options.decimal : true,
    integer: options && options.integer ? options.integer : true,
    space: options && options.space ? options.space : false,
    length: options && options.length ? options.length : true,
  };

  const config = currencies[_currency];
  if (use.space) {
    config.m = S;
    config.d = T;
  }

  let result = '';
  let parts;
  let partsLength;
  let reverse = '';
  let reverseAux = '';
  const reverseArr = [];
  let decimal;
  // two paths
  if (_value.includes(P) && !_value.includes(C)) {
    // js number syntax (?)
    parts = _value.split(P);
    partsLength = parts.length;
    const integer = reverseString(parts[0]);
    decimal = fixDecimals(parts[1]);
    let iLen = 0;
    for (; iLen < integer.length; iLen += 3) {
      reverseArr.push(integer.substr(iLen, 3));
      reverse += integer.substr(iLen, 3);
      if (iLen + 3 < integer.length && use.integer) {
        reverse += config.m;
      }
    }
    reverse = reverseString(reverse);

    result = reverse + (use.decimal ? config.d : '') + decimal;
  // } else if (_value.includes(C) || _value.includes(P)) {
  } else if (_value.includes(C) && _value.includes(P)) {
    // , separator
    let finalIndex = P;
    // comprobamos el primer indice > -1 del valor a tratar para C y P
    const pIndex = _value.indexOf(P);
    const cIndex = _value.indexOf(C);
    if (pIndex === -1) {
      finalIndex = C ;
    } else if (cIndex === -1) {
      finalIndex = P;
    } else if (pIndex !== -1 && cIndex !== -1) {
      finalIndex = pIndex < cIndex ? P : C;
    }
    parts = _value.split(finalIndex);

    partsLength = parts.length;
    // 1,234,567.89 - 1,234.56
    // tenemos que calcular cuantos config.m hay que poner
    if (partsLength === 2) {
      reverse = reverseString(parts[0]);
      // hacemos un substring
      let len = 0;
      for (; len < reverse.length; len += 3) {
        reverseAux += reverse.substr(len, 3);
        if (len + 3 < reverse.length && use.integer) {
          reverseAux += config.m;
        }
      }
      result = reverseAux + (use.decimal ? config.d : ' ') + parts[1];
    } else if (parts.length > 2) {
      let len = 0;
      for (; len < parts.length; len++) {
        const p = parts[len];
        if (p.includes(config.base.m)) {
          reverseAux += p.split(config.base.m)[0];
        } else if (p.includes(config.base.d)) {
          reverseAux +=  p.split(config.base.d)[0];
        } else {
          reverseAux += p;
        }
        if (len < parts.length - 1 && use.integer) {
          reverseAux += config.m;
        }
      }

      // TODO: revisar bien esto, y controlar las otras condiciones
      decimal = parts[parts.length - 1].split(config.base.d)[1] ?
                  fixDecimals(parts[parts.length - 1].split(config.base.d)[1]) :
                  fixDecimals(parts[parts.length - 1].split(config.base.m)[1]);
      result = reverseAux + (use.decimal ? config.d : ' ') + decimal;
    }
  } else {
    // no decimals
    reverse = reverseString(_value);
    let len = 0;
    for (; len < reverse.length; len += 3) {
      reverseAux += reverse.substr(len, 3);
      if (len + 3 < reverse.length && use.integer) {
        reverseAux += config.m;
      }
    }
    result = String(reverseString(reverseAux));
  }
  if (config.s) {
    if (config.placeBefore) {
      result = config.s + (use.space ? ' ' : '' ) + result;
    } else {
      result += (use.space ? ' ' : '' ) + config.s;
    }
  }
  return result;
}
