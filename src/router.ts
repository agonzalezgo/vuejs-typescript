import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/store-tests',
      name: 'store-tests',
      component: () => import(/* webpackChunkName: "store-tests" */ './views/ViewStore.vue'),
    },
    {
      path: '/progress',
      name: 'progress-view',
      component: () => import(/* webpackChunkName: "progress-view" */ './views/ViewProgress.vue'),
    },
    {
      path: '/dummy',
      name: 'dummy',
      component: () => import(/* webpackChunkName: "dummy" */ './views/ViewDummy.vue'),
    },
    // {
    //   path: '/map',
    //   name: 'map',
    //   component: () => import(/* webpackChunkName: "map" */ './views/MapView.vue'),
    // },
    {
      path: '/prueba',
      name: 'prueba-leaflet',
      component: () => import(/* webpackChunkName: "prueba-leaflet" */ './views/PruebaLeaflet.vue'),
    },
    {
      path: '/tooltip',
      name: 'view-tooltips',
      component: () => import(/* webpackChunkName: "view-tooltips" */ './views/ViewTooltips.vue'),
    },
    {
      path: '/vista',
      name: 'view-general',
      component: () => import(/* webpackChunkName: "view-general" */ './views/ViewGeneral.vue'),
    },
    {
      path: '/p',
      name: 'prueba2',
      component: () => import(/* webpackChunkName: "prueba2" */ './components/custom/prueba2.vue'),
    },
  ],
});
