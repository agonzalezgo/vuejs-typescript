export interface InputEvent extends Event {
  data: any;
  value: any;
  isComposing: boolean;
}

export interface InputEventTarget extends EventTarget {
  value: any;
}
