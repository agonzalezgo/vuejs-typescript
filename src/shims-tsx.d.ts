import Vue, { VNode } from 'vue';

declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    interface Element extends VNode {}
    // tslint:disable no-empty-interface
    interface ElementClass extends Vue {}
    interface IntrinsicElements {
      [elem: string]: any;
    }
  }
  // añadidos leaflet
  namespace L {
    export interface Map {}
    export interface TileLayer {}
    export interface Tooltip {}
    export interface Popup {}
    // export interface BoundsLiteral {}
    // export interface BoundsExpression {}
    // export interface LatLngExpression {}
    export interface Polyline {}
    export interface Polygon {}
    export interface Marker {}
    export interface MarkerOptions {}
    export interface Icon {}
    // export interface PointExpression {}
    export interface TileLayer {}
    export interface Rectangle {}
    // export interface LatLngBoundsExpression {}
    export interface MarkerCluster {}
  }
}
