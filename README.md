# prueba

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# DOCUMENTACION

## Pantalla 'About' */#/about*

Esta pantalla tiene:

- ejemplo de declaracion de variables en la clase del componente
- componentes hijo y como importar componentes (EmitComponent, PropsComponent...)
- uso de componentes tipo mixin (WithMixinsComponent)
- llamadas HTTP via Axios
- uso del store de vue (Vuex) con decoradores
- uso de un filter/pipe (currency)
- input customizable muy básico

## Pantalla 'Store tests' */#/store-tests*

- uso básico de ejemplo con Vuex

## Pantalla 'dummy' */#/dummy*

- uso de vue-dummy

## Pantalla 'Mapa Example.vue' */#/prueba*

- ejemplo de uso de leaflet con vue, uso de clusters
- localizacion de usuario

## Pantalla 'Tooltip' */#/tooltip*

- ejemplo de uso de la directiva v-tooltip y del componente v-popover

## Pantalla 'Progress components' */#/progress*

- uso de diferentes componentes para visualizar un proceso

## Pantalla 'vista' */#/vista*

- uso de store para cambiar el tema principal de la aplicacion (dark/light, muy básico)
- uso de servicios de locaclizacion

  - - - -

### axios & vue-axios para llamadas http
 -> HttpComponent.vue

### ts-debounce
 -> CustomInput.vue

### font-awesome icons
 -> https://www.npmjs.com/package/@fortawesome/vue-fontawesome

### vue-dummy

Sirve para rellenar elementos HTML con datos aleatorios
 -> https://github.com/paulcollett/vue-dummy#readme

 ### vue-datepicker + vue-moment

 -> https://github.com/mathieustan/vue-datepicker -> https://github.com/brockpetrie/vue-moment#readme

 ### generacion de componentes desde linea de comandos
  -> npm install -g vue-generate-component-typescript
  -> vgc --help
  -> https://github.com/Kamar-Meddah/vue-generate-component-typescript

  componente generado con *vgc* ~src/components/custom/prueba~